package com.myapp.authenticate.models;

public enum ERole {
	ROLE_USER,
    ROLE_ADMIN
}
