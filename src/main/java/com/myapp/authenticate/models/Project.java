package com.myapp.authenticate.models;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "project")
public class Project implements Serializable{

private static final long serialVersionUID = 1L;

public Project()
{
	
}


public Project(Long projectId, String projectName, String duration, Student student) {
	super();
	this.projectId = projectId;
	this.projectName = projectName;
	this.duration = duration;
}

@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
 private Long projectId;
 
 private String projectName;
 
 private String duration;
 
 

public Long getProjectId() {
	return projectId;
	
}

public void setProjectId(Long projectId) {
	this.projectId = projectId;
}

public String getProjectName() {
	return projectName;
}

public void setProjectName(String projectName) {
	this.projectName = projectName;
}

public String getDuration() {
	return duration;
}

public void setDuration(String duration) {
	this.duration = duration;
}

}
