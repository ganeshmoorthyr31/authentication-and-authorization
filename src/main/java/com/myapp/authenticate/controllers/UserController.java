package com.myapp.authenticate.controllers;

import java.util.List;
import com.myapp.authenticate.models.Student;
import com.myapp.authenticate.security.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/user")
public class UserController {
	
	    @Autowired
	    private UserService userService;

	    @PreAuthorize("hasRole('ROLE_ADMIN')")
	    @RequestMapping(value="/getallstudentdetails", method = RequestMethod.GET)
	    public List<Student> listallStudents(){
	        return userService.listallStudents();
	    }

	    @PreAuthorize("hasRole('ROLE_ADMIN')")
	    @RequestMapping(value = "/getstudentbyid/{id}", method = RequestMethod.GET)
	    public Student getStudentbyId(@PathVariable(value = "id") Long id){
	        return userService.getStudentbyId(id);
	    }

	    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
	    @RequestMapping(value="/addStudent", method = RequestMethod.POST)
	    public Student saveStudent(@RequestBody Student student){
	        return userService.saveStudent(student);

}
	    
}
