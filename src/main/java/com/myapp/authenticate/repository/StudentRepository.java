package com.myapp.authenticate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.myapp.authenticate.models.Student;
@Repository
public interface StudentRepository extends JpaRepository<Student, Long>{

}
