package com.myapp.authenticate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.myapp.authenticate.models.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {

}
