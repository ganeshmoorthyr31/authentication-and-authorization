package com.myapp.authenticate.security.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myapp.authenticate.models.Student;
import com.myapp.authenticate.repository.StudentRepository;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	StudentRepository studentRepository;
	
	@Override
	public List<Student> listallStudents() {
		return studentRepository.findAll();
	}

	@Override
	public Student getStudentbyId(Long id) {
		Student st = null;
		Optional<Student> student = studentRepository.findById(id);
		if(student.get()!=null)
	     st = (Student) student.get();
		return st;
	}

	@Override
	public Student saveStudent(Student student) {
		return studentRepository.save(student);
	}

}
