package com.myapp.authenticate.security.services;

import java.util.List;
import com.myapp.authenticate.models.Student;

public interface UserService {
	
	 public List<Student> listallStudents();
	 public Student getStudentbyId(Long id);
	 public Student saveStudent(Student student);

}
